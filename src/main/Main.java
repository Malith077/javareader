package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {

	static String[][] returnGridfromFile(String filename) {
		
		List<String[]> Values = new ArrayList<String[]>();
		try {
			//Copy the content of the file to a list
			FileInputStream fileInputStream = new FileInputStream(filename);
			String line = "";

			InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
			BufferedReader bufferReader = new BufferedReader(inputStreamReader);
			
			//Skipping starting characters
			bufferReader.mark(1);
			if (bufferReader.read() != 0xFEFF)
				bufferReader.reset();
			
			while((line = bufferReader.readLine()) != null) {
				String[] values = line.split(",");
				Values.add(values);
			}
			
			bufferReader.close();
		}
		catch(FileNotFoundException e) {
			System.out.print("An error occured - File not found");
			e.printStackTrace();
		}
		catch (IOException e) {
			System.out.print("IO exception occured");
			e.printStackTrace();
		}
		
		String[][] array = new String[Values.size()][];
		array = Values.toArray(array);
		return array;
	}
	
	static boolean wordSearch(String[][] wordBoard, String word, String direction) {
		if(word.isEmpty()) {
			throw new InputMismatchException("Input cannot be empty");
		}
		
		switch(direction) {
			case "H":
				horizontalSearch(wordBoard, word);
				break;
			case "V":
				verticalSearch(wordBoard, word);
				break;
			case "D":
				diagonalSearch(wordBoard, word);
				break;
			case "R":
				reverseSearch(wordBoard, word);
			default : direction = "Invalid input";
				break;
		}
		
		System.out.println(word);
		
		
		return false;
	}
	
	private static void printMessage(String word, int count) {
		//Prints word search results
		if(count > 0) {
			System.out.println("Word - "+ word + " has been found " + count + " times");
			return;
		}
		
		System.out.println("Your search string has no hits.");
	}
	
	private static void reverseSearch(String[][] wordBoard, String word) {
		int count = 0;

		for(int i = wordBoard.length - 1; i >= 0; i--) {
			for(int j = wordBoard[i].length - 1; j >= 0; j--) {
				if(wordBoard[i][j].toLowerCase().equals(word)) {
					count++;
				}
			}
		}
		
		printMessage(word, count);
	}

	private static void diagonalSearch(String[][] wordBoard, String word) {
		int rowcount = wordBoard.length;
		int colcount = wordBoard[0].length;
		int count = 0;
		for(int i = 0; i < rowcount; i++) {
			for(int row = i, col = 0; row >= 0 && col < colcount; row--, col++) {
				if(wordBoard[row][col].toLowerCase().equals(word)) {
					count++;
				}
			}
		}
		
		for(int j = 0; j < colcount; j++) {
			for(int row = rowcount -1, col = j; row >= 0 && col < colcount; row--, col++) {
				if(wordBoard[row][col].toLowerCase().equals(word.toLowerCase())) {
					count++;
				}
			}
		}
		
		printMessage(word, count);
	}

	private static void horizontalSearch(String[][] wordBoard, String word) {
		int count = 0;
		for(int j = 0; j < wordBoard.length; j++) {
			for(int i = 0; i < wordBoard[j].length; i++) {
				if(wordBoard[j][i].toLowerCase().equals(word)) {
					count++;
				}
			}
		}
		
		printMessage(word, count);
	}
	
	private static void verticalSearch(String[][] wordBoard, String word) {
		int count = 0;
		
		for(int i = 0; i < wordBoard[0].length; i++) {
			for(int j = 0; j < wordBoard.length; j++) {
				if(wordBoard[j][i].toLowerCase().equals(word)) {
					count++;
				}
			}
		}
		
		printMessage(word, count);
	}

	public static void main(String[] args) {
		var stringArray = returnGridfromFile("wordSearch.csv");

		for(int i = 0; i < stringArray.length; i++) {
			System.out.println(Arrays.toString(stringArray[i]));
		}
		
		String userInput = "";
		String searchType = "";
        Scanner scan = new Scanner(System.in);
        System.out.println("Welcome to word Search, follow the instructions below, and press 'q' to exit");
		while(!userInput.equals("q")){
			System.out.println("Enter something to search :");
			userInput = scan.nextLine();
			System.out.println("Enter search direction as H - horizontal, V - vertical, D - diagonal and R - reverse");
			searchType = scan.nextLine();
			wordSearch(stringArray, userInput.toLowerCase(), searchType);
		};
		System.out.println("Bye");
		scan.close();
	}

}
